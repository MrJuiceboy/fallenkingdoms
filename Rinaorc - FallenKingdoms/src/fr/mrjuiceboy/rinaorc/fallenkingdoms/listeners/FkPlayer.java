package fr.mrjuiceboy.rinaorc.fallenkingdoms.listeners;

import java.util.Map.Entry;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import fr.mrjuiceboy.rinaorc.fallenkingdoms.FallenKingdoms;
import fr.mrjuiceboy.rinaorc.fallenkingdoms.game.Game;
import fr.mrjuiceboy.rinaorc.fallenkingdoms.game.status.GameStatus;
import fr.mrjuiceboy.rinaorc.fallenkingdoms.utils.MessageManager;
import fr.mrjuiceboy.rinaorc.fallenkingdoms.utils.ScoreboardSign;

public class FkPlayer implements Listener {
	
	private FallenKingdoms pl;
	private int task;
	
	public FkPlayer(FallenKingdoms pl){
		this.pl = pl;
	}
	
	@EventHandler
	public void onPlayerQuit(PlayerQuitEvent event){
		Player player = event.getPlayer();
		event.setQuitMessage("");
		
		for(Entry<Player, ScoreboardSign> sign : this.pl.boards.entrySet()){
			sign.getValue().setLine(1, ChatColor.WHITE + "Joueurs: " + ChatColor.GREEN + Bukkit.getServer().getOnlinePlayers().size() + "/" + Bukkit.getServer().getMaxPlayers());
		}
		
		if(this.pl.boards.containsKey(player)){
			this.pl.boards.get(player).destroy();
		}
	}
	
	@EventHandler
	public void onJoin(PlayerJoinEvent e){
		Player p = e.getPlayer();
		int playercount = Bukkit.getServer().getOnlinePlayers().size();
		
		if(Game.getStatus() == GameStatus.LOBBY) {
			Location location = new Location(Bukkit.getWorld("world"), 100.500, 220, 9.500, -179, 0);
			p.teleport(location);
			p.setGameMode(GameMode.ADVENTURE);
			p.setWalkSpeed(0.26F);
			p.setFlySpeed(0.1F);
			p.setFlying(false);
			p.setAllowFlight(false);
			p.setFoodLevel(20);
			p.getInventory().clear();
			p.setCanPickupItems(false);
			p.setFlying(false);
			p.setLevel(0);
			p.setExp(0F);
			p.setMaxHealth(20D);
			p.setHealth(20D);
			p.getActivePotionEffects().clear();
			p.setDisplayName(ChatColor.GRAY + p.getName());
		    p.setPlayerListName(ChatColor.GRAY + p.getName());
		}else if(Game.getStatus() == GameStatus.STARTED){
			p.kickPlayer(MessageManager.getPrefix() + "§cLa partie à déjà commencer !");
			// Ou set game mode spec
		}else if(Game.getStatus() == GameStatus.FINISH){
			p.kickPlayer(MessageManager.getPrefix() + "§cLa partie vient de terminer !");
		}
		
		e.setJoinMessage(ChatColor.GRAY + p.getName() + ChatColor.YELLOW + " vient de rejoindre la partie (" + ChatColor.AQUA + Bukkit.getServer().getOnlinePlayers().size() + ChatColor.YELLOW + "/" + ChatColor.AQUA + Bukkit.getServer().getMaxPlayers() + ChatColor.YELLOW + ")!");
		if(Game.getStatus() == GameStatus.LOBBY) {
			ItemStack selector = new ItemStack(Material.COMPASS);
	        ItemMeta meta = selector.getItemMeta();	        
	        meta.setDisplayName(ChatColor.GOLD + "Menu Principale");
	        selector.setItemMeta(meta);
	        p.getInventory().setItem(4, selector);			
			p.updateInventory();
			
		    ScoreboardSign scoreboard = new ScoreboardSign(p, "§e§lFallenKingdoms");
		    scoreboard.create();
		    scoreboard.setLine(0, " ");
		    scoreboard.setLine(1, ChatColor.WHITE + "Joueurs: " + ChatColor.GREEN + Bukkit.getServer().getOnlinePlayers().size() + "/" + Bukkit.getServer().getMaxPlayers());
		    scoreboard.setLine(2, "  ");
		    //Ligne 3 Check-up player required
		    scoreboard.setLine(4, "   ");
		    scoreboard.setLine(5, ChatColor.WHITE + "Map: " + ChatColor.GREEN + "Tropico");
		    scoreboard.setLine(6, "    ");
		    scoreboard.setLine(7, ChatColor.GOLD + "play.rinaorc.com");
		    this.pl.boards.put(p, scoreboard);
		    
		    if(playercount >= 2) {//2 mini pour start
		    	task = Bukkit.getScheduler().scheduleSyncRepeatingTask(pl, new Runnable(){
		    		int countdown = 61; //61 pour start		    		
					@Override
					public void run() {
		    			countdown--;
		    			for(Entry<Player, ScoreboardSign> sign : pl.boards.entrySet()){
		    				sign.getValue().setLine(1, ChatColor.WHITE + "Joueurs: " + ChatColor.GREEN + Bukkit.getServer().getOnlinePlayers().size() + "/" + Bukkit.getServer().getMaxPlayers());
		    				sign.getValue().setLine(3, ChatColor.WHITE + "Début dans " + ChatColor.GREEN + countdown + "s");
		    			}
		    			if(this.countdown == 0) {
		    				Bukkit.getScheduler().cancelTask(task);	
							Game.start();
		    			}		    			
		    			if((countdown < 5) && (countdown != 0)){
		    				for (Player pls : Bukkit.getOnlinePlayers()){
		    					//TitleAPI
		    					pls.getPlayer().playSound(pls.getPlayer().getLocation(), Sound.SUCCESSFUL_HIT, 1.0F, 1.0F);
		    				}
		    			}		    			
		    			if(countdown == 10) {
		    				for (Player pls : Bukkit.getOnlinePlayers()){
		    					//TitleAPI
		    					pls.getPlayer().playSound(pls.getPlayer().getLocation(), Sound.SUCCESSFUL_HIT, 1.0F, 1.0F);
		    				}
		    			}		    			
		    			if(countdown == 20) {
		    				for (Player pls : Bukkit.getOnlinePlayers()){
		    					//TitleAPI
		    					pls.getPlayer().playSound(pls.getPlayer().getLocation(), Sound.SUCCESSFUL_HIT, 1.0F, 1.0F);
		    				}
		    			}
		    			if(countdown == 30) {
		    				for (Player pls : Bukkit.getOnlinePlayers()){
		    					//TitleAPI
		    					pls.getPlayer().playSound(pls.getPlayer().getLocation(), Sound.SUCCESSFUL_HIT, 1.0F, 1.0F);
		    				}
		    			}
					}
				}, 20L, 20L);
		    }else if(playercount < 2) {
		    	if(this.pl.boards.containsKey(p)) {
		    		if(Game.getStatus() == GameStatus.LOBBY) {		    			
		    			for(Entry<Player, ScoreboardSign> sign : this.pl.boards.entrySet()){
		    				sign.getValue().setLine(1, ChatColor.WHITE + "Joueurs: " + ChatColor.GREEN + Bukkit.getServer().getOnlinePlayers().size() + "/" + Bukkit.getServer().getMaxPlayers());
		    				sign.getValue().setLine(3, ChatColor.WHITE + "En attente...");
		    			}
	            	}
		    	}
		    }
		}
	}
	
	@EventHandler
	public void onChat(AsyncPlayerChatEvent e){
		Player p = e.getPlayer();
	    String msg = e.getMessage();
	    e.setCancelled(true);
	    if(Game.getTeamForPlayer(p) == Game.teamRouge) {
	    	if(msg.startsWith("!")) {
	    		Bukkit.broadcastMessage(ChatColor.RED + "(Tous)" + ChatColor.GOLD + p.getDisplayName() + ChatColor.RED + " ===> " + ChatColor.BOLD + msg.replaceFirst("!", " "));
	    	}else {
	    		for (Player pls : Bukkit.getOnlinePlayers()) {
	    			if(Game.getTeamForPlayer(p) == Game.teamRouge) {
	    				pls.sendMessage(ChatColor.RED + "(Equipe)" + ChatColor.GOLD + p.getDisplayName() + ChatColor.RED + " ===> " + ChatColor.BOLD + msg);
	    			}
	    		}
	    	}
	    }
	    if(Game.getTeamForPlayer(p) == Game.teamBleu) {
	    	if(msg.startsWith("!")) {
	    		Bukkit.broadcastMessage(ChatColor.BLUE + "(Tous)" + ChatColor.GOLD + p.getDisplayName() + ChatColor.BLUE + " ===> " + ChatColor.BOLD + msg.replaceFirst("!", " "));
	    	}else {
	    		for (Player pls : Bukkit.getOnlinePlayers()) {
	    			if(Game.getTeamForPlayer(p) == Game.teamBleu) {
	    				pls.sendMessage(ChatColor.BLUE + "(Equipe)" + ChatColor.GOLD + p.getDisplayName() + ChatColor.BLUE + " ===> " + ChatColor.BOLD + msg);
	    			}
	    		}
	    	}
	    }
	}
	
	public void onFood(FoodLevelChangeEvent e){
		if(Game.getStatus() == GameStatus.LOBBY || Game.getStatus() == GameStatus.FINISH) {
			e.setCancelled(true);
			e.setFoodLevel(20);		
	    }else if (Game.getStatus() == GameStatus.STARTED) {	   
	    	e.setCancelled(false);
		}
	}
	
	@EventHandler
	  public void onDamage(EntityDamageEvent e) {		
		if(Game.getStatus() == GameStatus.LOBBY || Game.getStatus() == GameStatus.FINISH) {
			if(e.getEntity() instanceof Player) {
				e.setDamage(0.0F);
				e.setCancelled(true);
			}
			((Player) e.getEntity()).setHealth(((Player) e.getEntity()).getMaxHealth());
	    }else if (Game.getStatus() == GameStatus.STARTED) {	   
	    	e.setCancelled(false);
		}
	}
	
	@EventHandler
	public void onBlock(BlockBreakEvent e) {
		if(Game.getStatus() == GameStatus.LOBBY || Game.getStatus() == GameStatus.FINISH) {
	    	e.setCancelled(true);
	    }else if (Game.getStatus() == GameStatus.STARTED) {	   
	    	e.setCancelled(false);
	    }
	}
	
	@EventHandler
	public void onBlock(BlockPlaceEvent e) {
		Player p = e.getPlayer();
	    Block b = e.getBlock();
	    if(Game.getStatus() == GameStatus.LOBBY || Game.getStatus() == GameStatus.FINISH) {
	    	e.setCancelled(true);
	    }else if (Game.getStatus() == GameStatus.STARTED) {	    
		    if(201 > b.getLocation().getX() &&
		    		157 < b.getLocation().getX() &&
		    		107 > b.getLocation().getY() &&
		    		88 < b.getLocation().getY() &&
		    		87 > b.getLocation().getZ() &&
		    		43 < b.getLocation().getZ()) {
		    	if(!(Game.getTeamForPlayer(p) == Game.teamRouge) &&
		    			(b.getType() != Material.TNT) && (b.getType() != Material.TORCH) && (b.getType() != Material.REDSTONE_TORCH_ON) && (b.getType() != Material.REDSTONE_TORCH_OFF) && (b.getType() != Material.FIRE)) {
		    		b.setType(Material.AIR);
		    		p.sendMessage(MessageManager.getPrefix() + ChatColor.RED + "Vous n'avez pas le droit de poser ce block ici !");
		    	}
		    }	    
		    if(95 > b.getLocation().getX() &&
		    		51 < b.getLocation().getX() &&
		    		108 > b.getLocation().getY() &&
		    		89 < b.getLocation().getY() &&
		    		200 > b.getLocation().getZ() &&
		    		156 < b.getLocation().getZ()) {
		    	if(!(Game.getTeamForPlayer(p) == Game.teamBleu) &&
		    			(b.getType() != Material.TNT) && (b.getType() != Material.TORCH) && (b.getType() != Material.REDSTONE_TORCH_ON) && (b.getType() != Material.REDSTONE_TORCH_OFF) && (b.getType() != Material.FIRE)) {
		    		b.setType(Material.AIR);
		    		p.sendMessage(MessageManager.getPrefix() + ChatColor.RED + "Vous n'avez pas le droit de poser ce block ici !");
		    	}
		    }
	    }
	}
	
	@EventHandler
	public void onMove(PlayerMoveEvent e){
		Player p = e.getPlayer();
		if(Game.getTeamForPlayer(p) == Game.getTeamBleu()) {
			p.setDisplayName(ChatColor.BLUE + "[Bleu]" + ChatColor.DARK_BLUE + p.getName());
		    p.setPlayerListName(ChatColor.BLUE + "[Bleu]" + ChatColor.DARK_BLUE + p.getName());
		}else if (Game.getTeamForPlayer(p) == Game.getTeamRouge()) {
			p.setDisplayName(ChatColor.RED + "[Rouge]" + ChatColor.DARK_RED + p.getName());
		    p.setPlayerListName(ChatColor.RED + "[Rouge]" + ChatColor.DARK_RED + p.getName());
		}else if (Game.getTeamForPlayer(p) == Game.getTeamVert()) {
			p.setDisplayName(ChatColor.GREEN + "[Vert]" + ChatColor.DARK_GREEN + p.getName());
		    p.setPlayerListName(ChatColor.GREEN + "[Vert]" + ChatColor.DARK_GREEN + p.getName());
		}else if (Game.getTeamForPlayer(p) == Game.getTeamJaune()) {
			p.setDisplayName(ChatColor.YELLOW + "[Jaune]" + ChatColor.YELLOW + p.getName());
		    p.setPlayerListName(ChatColor.YELLOW + "[Jaune]" + ChatColor.YELLOW + p.getName());
		}
		
		if(201 > p.getLocation().getX() &&
	    		157 < p.getLocation().getX() &&
	    		107 > p.getLocation().getY() &&
	    		88 < p.getLocation().getY() &&
	    		87 > p.getLocation().getZ() &&
	    		43 < p.getLocation().getZ()) {
			p.sendMessage(ChatColor.GREEN + "Vous entrez dans la base:" + ChatColor.DARK_RED + " Rouge");
		}
		
		if(95 > p.getLocation().getX() &&
	    		51 < p.getLocation().getX() &&
	    		108 > p.getLocation().getY() &&
	    		89 < p.getLocation().getY() &&
	    		200 > p.getLocation().getZ() &&
	    		156 < p.getLocation().getZ()) {
			p.sendMessage(ChatColor.GREEN + "Vous entrez dans la base:" + ChatColor.BLUE + " Bleu");
		}
	}
}
