package fr.mrjuiceboy.rinaorc.fallenkingdoms.listeners;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;

import fr.mrjuiceboy.rinaorc.fallenkingdoms.game.Game;
import fr.mrjuiceboy.rinaorc.fallenkingdoms.game.teams.menu.FkMenu;
import fr.mrjuiceboy.rinaorc.fallenkingdoms.utils.MessageManager;

public class FkMenuEvent implements Listener {
	
	@EventHandler
	public void onPlayerUse(PlayerInteractEvent event){
	    Player p = event.getPlayer();	 
	    if(p.getItemInHand().getType() == Material.COMPASS){
	    	FkMenu.openMenu(p);
	    }	    
	}
	
	@EventHandler
	public void onInventoryClick(InventoryClickEvent e) {  
		Player p = (Player) e.getWhoClicked();	 
		if(p.getItemInHand().getType() == Material.COMPASS){
	    	FkMenu.openMenu(p);
	    }
		
    	 if (!e.getInventory().getName().equals("�6Menu Principale") || e.getCurrentItem() == null || !e.getCurrentItem().hasItemMeta() || !e.getCurrentItem().getItemMeta().hasDisplayName())
    	        return;         	
        	e.setCancelled(true);        	
        	
        	switch (e.getCurrentItem().getItemMeta().getDisplayName()) {
        	
        	 case " ":
        		 p.closeInventory();												
           	 		break;
        	
             case "�9Rejoindre l'�quipe bleu !": 
            	p.setDisplayName(ChatColor.BLUE + "[Bleu]" + ChatColor.DARK_BLUE + p.getName());
 		        p.setPlayerListName(ChatColor.BLUE + "[Bleu]" + ChatColor.DARK_BLUE + p.getName());
 		        Game.getTeamRouge().removePlayer(p);
 		        Game.getTeamBleu().addPlayer(p);
 		        p.sendMessage(MessageManager.getPrefix() + ChatColor.GRAY + "Vous avez rejoin la team :" + ChatColor.BLUE + " bleu");
 		        FkMenu.openMenu(p);
                 	break;  
                 	
             case "�cRejoindre l'�quipe rouge !": 
            	 p.setDisplayName(ChatColor.RED + "[Rouge]" + ChatColor.DARK_RED + p.getName());
                 p.setPlayerListName(ChatColor.RED + "[Rouge]" + ChatColor.DARK_RED + p.getName());
                Game.getTeamBleu().removePlayer(p);
  		        Game.getTeamRouge().addPlayer(p);
  		        p.sendMessage(MessageManager.getPrefix() + ChatColor.GRAY + "Vous avez rejoin la team :" + ChatColor.RED + " rouge");
  		        FkMenu.openMenu(p);
                  	break;  
            	 	
			default:
				
                 break;
        }	
	}
}
