package fr.mrjuiceboy.rinaorc.fallenkingdoms.game.teams;

import java.util.ArrayList;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.Scoreboard;

public class GameTeam{
	
	private ArrayList<Player> player = new ArrayList<Player>();
	private String name;
	private String displayName;
	private ChatColor color;
	private Scoreboard scoreboard;

	public GameTeam(String name, String displayName, ChatColor color, Scoreboard scoreboard){
		this.name = name;
		setDisplayName(displayName);
		setColor(color);
		this.scoreboard = scoreboard;
		getScoreboard().registerNewTeam(getName());
		getScoreboard().getTeam(getName()).setDisplayName(getDisplayName());
		getScoreboard().getTeam(getName()).setAllowFriendlyFire(true);
		getScoreboard().getTeam(getName()).setCanSeeFriendlyInvisibles(true);
		getScoreboard().getTeam(getName()).setPrefix(getColor()+"");
	}

	public String getName(){
		return name;
	}
	
	public String getDisplayName(){
		return displayName;
	}
	
	public void setDisplayName(String displayName){
		this.displayName = displayName;
	}
	
	public ChatColor getColor(){
		return color;
	}
	
	public void setColor(ChatColor color){
		this.color = color;
	}
	
	public Scoreboard getScoreboard(){
		return scoreboard;
	}
	
	public ArrayList<Player> getPlayers(){
		return player;
	}
	
	public void addPlayer(Player player){
		if(!getPlayers().contains(player)){
			getPlayers().add(player);
			getScoreboard().getTeam(getName()).addEntry(player.getName());
		}
	}
	
	public void removePlayer(Player player){
		if(getPlayers().contains(player)){
			getPlayers().remove(player);
			getScoreboard().getTeam(getName()).removeEntry(player.getName());
		}
	}
	
	public void teleport(Location loc){
		for(Player pl : getPlayers()){
			pl.teleport(loc);
		}
	}
}
