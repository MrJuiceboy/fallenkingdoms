package fr.mrjuiceboy.rinaorc.fallenkingdoms.game.teams.menu;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import fr.mrjuiceboy.rinaorc.fallenkingdoms.game.Game;
import fr.mrjuiceboy.rinaorc.fallenkingdoms.game.teams.GameTeam;

public class FkMenu {

	public static void openMenu(Player p){
		Inventory inv = Bukkit.createInventory(null, 27, "§6Menu Principale");
		
		ItemStack blue = new ItemStack(Material.LAPIS_BLOCK, 1);
	    ItemMeta blueMeta = blue.getItemMeta();
	    blueMeta.setDisplayName("§9Rejoindre l'équipe bleu !");
	    ArrayList<String> blueLore = new ArrayList<String>();
	    blueLore.add(ChatColor.GRAY + "Menbres présent: ");
	    for (GameTeam team : Game.teams) {
	    	for (Player player : team.getPlayers()) {
	    		blueLore.add("§6- " + player.getDisplayName());
	    	}
	    }
	    blueMeta.setLore(blueLore);
	    blue.setItemMeta(blueMeta);
	    
	    ItemStack red = new ItemStack(Material.REDSTONE_BLOCK, 1);
	    ItemMeta redMeta = red.getItemMeta();
	    redMeta.setDisplayName("§cRejoindre l'équipe rouge !");
	    ArrayList<String> redLore = new ArrayList<String>();
	    redLore.add(ChatColor.GRAY + "Menbres présent: ");
	    for (GameTeam team : Game.teams) {
	    	for (Player player : team.getPlayers()) {
	    		redLore.add("§6- " + player.getDisplayName());
	    	}
	    }
	    redMeta.setLore(redLore);
	    red.setItemMeta(redMeta);
	    
	    inv.setItem(0, blue);
	    inv.setItem(4, red);
	    p.openInventory(inv);
	}
}
