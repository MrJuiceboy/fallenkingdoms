package fr.mrjuiceboy.rinaorc.fallenkingdoms.game;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftEntity;
import org.bukkit.entity.EnderCrystal;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityTargetEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;

import fr.mrjuiceboy.rinaorc.fallenkingdoms.FallenKingdoms;
import fr.mrjuiceboy.rinaorc.fallenkingdoms.game.status.GameStatus;
import fr.mrjuiceboy.rinaorc.fallenkingdoms.game.teams.GameTeam;
import net.minecraft.server.v1_8_R3.Entity;
import net.minecraft.server.v1_8_R3.NBTTagCompound;

public class Game implements Listener{
	
	public static ArrayList<GameTeam> teams = new ArrayList<GameTeam>();
	public static GameTeam teamBleu;
	public static GameTeam teamRouge;
	public static GameTeam teamVert;
	public static GameTeam teamJaune;
	public static GameStatus status;
	public FallenKingdoms pl;
	public int maxPlayers;
	private Scoreboard scoreboard;
	@SuppressWarnings("unused")
	private Objective objective;
	private String objectiveName;
	
	//private int CrystalBleuHeart = 300;
    //private int CrystalRougeHeart = 300;
	
	public Game(FallenKingdoms pl, int maxPlayers, GameStatus status) {
		this.pl = pl;
		setMaxPlayers(maxPlayers);
		setStatus(status);
		this.objectiveName = "CrystalDefense";
	    setScoreboard(Bukkit.getScoreboardManager().getNewScoreboard());
	    Objective o = getScoreboard().registerNewObjective(getObjectiveName(), "dummy");
	    setObjective(o);
		setTeamBleu(new GameTeam("blue", ChatColor.BLUE + "Bleu", ChatColor.BLUE, getScoreboard()));
	    setTeamRouge(new GameTeam("red", ChatColor.RED + "Rouge", ChatColor.RED, getScoreboard()));
	    setTeamVert(new GameTeam("green", ChatColor.GREEN + "Vert", ChatColor.GREEN, getScoreboard()));
	    setTeamJaune(new GameTeam("yellow", ChatColor.YELLOW + "Jaune", ChatColor.YELLOW, getScoreboard()));
		teams.add(getTeamBleu());
		teams.add(getTeamRouge());	
		teams.add(getTeamVert());
		teams.add(getTeamJaune());	
	}
	
	public void setObjective(Objective objective){
	    this.objective = objective;
	  }
	
	public String getObjectiveName(){
	    return this.objectiveName;
	  }
	
	public void setScoreboard(Scoreboard scoreboard){
	    this.scoreboard = scoreboard;
	  }
	
	public Scoreboard getScoreboard(){
	    return this.scoreboard;
	  }
	
	public static GameStatus getStatus(){
	    return status;
	  }
	
	public static void setStatus(GameStatus status){
	    Game.status = status;
	  }
	
	public void setMaxPlayers(int maxPlayers){
	    this.maxPlayers = maxPlayers;
	  }
	
	public void setTeamBleu(GameTeam teamBleu){
	    Game.teamBleu = teamBleu;
	  }
	
	public void setTeamRouge(GameTeam teamRouge){
	    Game.teamRouge = teamRouge;
	  }
	
	public void setTeamVert(GameTeam teamVert){
	    Game.teamVert = teamVert;
	  }
	
	public void setTeamJaune(GameTeam teamJaune){
	    Game.teamJaune = teamJaune;
	  }
	
	public static GameTeam getTeamBleu(){
	    return teamBleu;
	  }
	
	public static GameTeam getTeamRouge(){
	    return teamRouge;
	  }
	
	public static GameTeam getTeamVert(){
	    return teamVert;
	  }
	
	public static GameTeam getTeamJaune(){
	    return teamJaune;
	  }
	
	public static GameTeam getTeamForPlayer(Player p) {
	    for (GameTeam team : teams) {
	      if (team.getPlayers().contains(p)) {
	        return team;
	      }
	    }
	    return null;
	  }
	
	public static void start(){
		setStatus(GameStatus.STARTED);
		
		for (GameTeam team : teams)
	    {
	      for (Player player : team.getPlayers())
	      {
	        player.getInventory().clear();
	        player.getInventory().setHelmet(new ItemStack(Material.AIR));
	        player.getInventory().setChestplate(new ItemStack(Material.AIR));
	        player.getInventory().setLeggings(new ItemStack(Material.AIR));
	        player.getInventory().setBoots(new ItemStack(Material.AIR));
	        player.setFoodLevel(20);
	        player.setExhaustion(5.0F);
	        
	        Location locbluebase = new Location(Bukkit.getWorld("world"), 79.500, 88, 179.500);
	        Location locredbase = new Location(Bukkit.getWorld("world"), 185.500, 87, 66.500);
	        
	        if(Game.getTeamForPlayer(player) == Game.teamRouge) {
		    	 player.teleport(locredbase);
	        }else if(Game.getTeamForPlayer(player) == Game.teamBleu) {
	        	 player.teleport(locbluebase);
	        }
	      }
	    }
		
		EnderCrystal npcCrystalRouge = (EnderCrystal) Bukkit.getWorld("world").spawnEntity(new Location(Bukkit.getWorld("world"), 180.500, 87, 66.500), EntityType.ENDER_CRYSTAL);	
		Entity nmsCrystalRouge = ((CraftEntity) npcCrystalRouge).getHandle();
		nmsCrystalRouge.setCustomName(ChatColor.GRAY + "Crystal " + ChatColor.RED + "[Rouge]");
		nmsCrystalRouge.setCustomNameVisible(false);
		
		NBTTagCompound tagCrystalRouge = nmsCrystalRouge.getNBTTag();
		if(tagCrystalRouge == null){
			tagCrystalRouge = new NBTTagCompound();	
		}
		
		EnderCrystal npcCrystalBleu = (EnderCrystal) Bukkit.getWorld("world").spawnEntity(new Location(Bukkit.getWorld("world"), 74.500, 88, 179.500), EntityType.ENDER_CRYSTAL);	
		Entity nmsCrystalBleu = ((CraftEntity) npcCrystalBleu).getHandle();
		nmsCrystalBleu.setCustomName(ChatColor.GRAY + "Crystal " + ChatColor.RED + "[Bleu]");
		nmsCrystalBleu.setCustomNameVisible(false);
		
		NBTTagCompound tagCrystalBleu = nmsCrystalBleu.getNBTTag();
		if(tagCrystalBleu == null){
			tagCrystalBleu = new NBTTagCompound();	
		}
		
		//Lancement des task de check-up de la vie des crystal
	}
	
	@EventHandler
	public void onEntityDamageEvent(EntityDamageByEntityEvent e){
		if(Game.getStatus() == GameStatus.LOBBY || Game.getStatus() == GameStatus.FINISH) {
			e.setCancelled(true);
	    }else if (Game.getStatus() == GameStatus.STARTED) {	
	    	e.setCancelled(false);
		}
	}
	
	@EventHandler
	public void onPickupItem(PlayerPickupItemEvent e) {
		if(Game.getStatus() == GameStatus.LOBBY || Game.getStatus() == GameStatus.FINISH) {
			e.setCancelled(true);
	    }else if (Game.getStatus() == GameStatus.STARTED) {	
	    	e.setCancelled(false);
		}
	}
	
	@EventHandler
    public void onEntityTarget(EntityTargetEvent e) {
		if(Game.getStatus() == GameStatus.LOBBY || Game.getStatus() == GameStatus.FINISH) {
			e.setCancelled(true);
	    }else if (Game.getStatus() == GameStatus.STARTED) {	
	    	e.setCancelled(false);
		}
    }
	
	@EventHandler
	public void onEntityDamage(EntityDamageEvent event) {
		if(Game.getStatus() == GameStatus.LOBBY || Game.getStatus() == GameStatus.FINISH) {
			event.setCancelled(true);
		}else if (Game.getStatus() == GameStatus.STARTED) {	   
			event.setCancelled(false);
		}
	}
	
	@EventHandler
	public void onCreatureSpawn(CreatureSpawnEvent e) {
		if(Game.getStatus() == GameStatus.LOBBY || Game.getStatus() == GameStatus.FINISH) {
			if(e.getEntity().getType() == EntityType.CREEPER || e.getEntity().getType() == EntityType.ZOMBIE || e.getEntity().getType() == EntityType.SKELETON || e.getEntity().getType() == EntityType.SPIDER){
				e.setCancelled(true);
			}
	    }else if (Game.getStatus() == GameStatus.STARTED) {	   
	    	e.setCancelled(false);
		}
	}
}
