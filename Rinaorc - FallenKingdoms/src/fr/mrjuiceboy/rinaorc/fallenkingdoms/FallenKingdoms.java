package fr.mrjuiceboy.rinaorc.fallenkingdoms;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.entity.Animals;
import org.bukkit.entity.Creature;
import org.bukkit.entity.EnderCrystal;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Monster;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scoreboard.Scoreboard;

import fr.mrjuiceboy.rinaorc.fallenkingdoms.game.Game;
import fr.mrjuiceboy.rinaorc.fallenkingdoms.game.status.GameStatus;
import fr.mrjuiceboy.rinaorc.fallenkingdoms.listeners.FkMenuEvent;
import fr.mrjuiceboy.rinaorc.fallenkingdoms.listeners.FkPlayer;
import fr.mrjuiceboy.rinaorc.fallenkingdoms.utils.ScoreboardSign;

public class FallenKingdoms extends JavaPlugin {
	
	/*
     * FIELD
     */
	public PluginManager pm;
	public Scoreboard sb;
	public Map<Player, ScoreboardSign> boards = new HashMap<>();
	
	public void onEnable(){
		registerFields();
		registerListeners();
		
		for(World world : Bukkit.getWorlds()){
			for(Entity entity : world.getEntities()){
				if(entity instanceof Creature || entity.getType() == EntityType.VILLAGER || entity instanceof Animals || entity instanceof Monster || entity instanceof EnderCrystal) entity.remove();
			}
		}
		
		/*
		 * Temporaire
		 */
		this.sb = Bukkit.getScoreboardManager().getNewScoreboard();
		@SuppressWarnings("unused")
		Game game = new Game(this, 10, GameStatus.LOBBY);
		//Game.setStatus(GameStatus.LOBBY);
	}
	
	/**
     * Modifier les fields principaux
     */
    public void registerFields() {
        pm = getServer().getPluginManager();
    }
	
	/**
     * Enregistrer les listeners
     * @param manager
     */
    public void registerListeners() {
    	PluginManager pm = this.getServer().getPluginManager();	
    	pm.registerEvents(new FkPlayer(this), this);
    	pm.registerEvents(new FkMenuEvent(), this);
    }
    
    public void onDisable() {
    	
    }

}
